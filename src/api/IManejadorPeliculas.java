package api;

import model.data_structures.ILista;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;

/**
 * Interface de servicios que presta el manejador de pel�culas <br>
 * El a�o de referencia inicial para los servicios debe ser el primero disponible en la aplicaci�n (1950)
 */
public interface IManejadorPeliculas {
	
	/**
	 * Inicializa el manejador de pel�culas
	 * @param archivoPeliculas archivo en formato csv con pel�culas
	 */
	public void cargarArchivoPeliculas(String archivoPeliculas);
	
	/**
	 * Devuelve una lista de pel�culas cuyo t�tulo incluya la subcadena busqueda
	 * @param busqueda subcadena a buscar en los t�tulos
	 * @return Lista con pel�culas cuyo t�tulo incluye la subcadena busqueda
	 */
	public ILista<VOPelicula> darListaPeliculas(String busqueda);
	
	/**
	 * Devuelve las pel�culas que fueron publicadas en el a�o agno, 
	 * el a�o queda seleccionado como referencia si esta entre el rango manejado por la aplicaci�n. 
	 * @param agno a�o para el cu�l se buscan las pel�culas
	 * @return Lista de pel�culas registradas que fueron publicadas en el a�o agno
	 */
	public ILista<VOPelicula> darPeliculasAgno(int agno);

	/**
	 * Devuelve las pel�culas que fueron publicadas en el a�o siguiente al seleccionado como referencia, 
	 * el a�o de referencia se cambia al siguiente a�o, si no hay un a�o siguiente retorna null. 
	 * @return VOAgnoPelicula con el a�o y las pel�culas que pertenecen al a�o siguiente, null si no hay a�o anterior
	 */
	
	public VOAgnoPelicula darPeliculasAgnoSiguiente();
	
	/**
	 * Devuelve las pel�culas que fueron publicadas en el a�o anterior al seleccionado como referencia, 
	 * el a�o de referencia se cambia al anterior a�o, si no hay un a�o anterior retorna null. 
	 * @return VOAgnoPelicula con el a�o y las pel�culas que pertenecen al a�o anterior, null si no hay a�o anterior
	 */
	public VOAgnoPelicula darPeliculasAgnoAnterior();
}
