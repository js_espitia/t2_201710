package model.logic;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.Iterator;

import model.data_structures.ILista;
import model.data_structures.ListaDobleEncadenada;
import model.data_structures.ListaEncadenada;
import model.data_structures.NodoDoble;
import model.vo.VOAgnoPelicula;
import model.vo.VOPelicula;
import api.IManejadorPeliculas;

public class ManejadorPeliculas implements IManejadorPeliculas {

	private ILista<VOPelicula> misPeliculas;

	private ILista<VOAgnoPelicula> peliculasAgno;


	@Override
	public void cargarArchivoPeliculas(String archivoPeliculas) {

		// TODO Auto-generated method stub
		misPeliculas = null;
		FileReader arg0;
		try {
			arg0 = new FileReader(new File(archivoPeliculas));
			BufferedReader in = new BufferedReader(arg0);

			misPeliculas = new ListaEncadenada<VOPelicula>(null);
			peliculasAgno = new ListaDobleEncadenada<VOAgnoPelicula>(null);

			for(int i = 1950; i < 2017 ; i++){
				VOAgnoPelicula actual = new VOAgnoPelicula();
				actual.setAgno(i);
				actual.setPeliculas(new ListaDobleEncadenada<VOPelicula>(null));
				peliculasAgno.agregarElementoFinal(actual);
			}

			String line = in.readLine();
			line = in.readLine();
			while(line != null){
				String[] info = new String[4];
				String  name, genres;
				if(line.indexOf('"') != -1){				
					name = line.substring(line.indexOf('"')+1,line.lastIndexOf('"')).trim();
					line = line.substring(0, line.indexOf('"'))+line.substring(line.lastIndexOf('"')+1, line.length());
					info = line.split(",");
					genres = info[1];
				}else{
					info = line.split(",");
					name = info[1].trim();
					genres = info[2];
				}
				String[] generosArr = genres.split("|");
				String titulo = name.substring(0, name.length()-7);
				String anio = "0";
				if(name.charAt(name.length()-1)==')'){
					if(name.charAt(name.length()-6)=='('){
						anio = name.substring(name.length()-5, name.length()-1);
					}
					else if(name.charAt(name.length()-6)=='-'){
						anio = name.substring(name.length()-10, name.length()-6);
					}
					else if(name.charAt(name.length()-2)=='-'){
						anio = name.substring(name.length()-6, name.length()-2);
					}
				}
				ILista<String> generos = new ListaDobleEncadenada<>(null);
				for(int i=0 ; i<generosArr.length ; i++){
					generos.agregarElementoFinal(generosArr[i]);
				}

				VOPelicula actual = new VOPelicula();
				actual.setTitulo(titulo);
				actual.setAgnoPublicacion(Integer.parseInt(anio));
				actual.setGenerosAsociados(generos);
				misPeliculas.agregarElementoFinal(actual);
				
				if(actual.getAgnoPublicacion()>1950)
					peliculasAgno.darElemento(actual.getAgnoPublicacion()-1950).getPeliculas().agregarElementoFinal(actual);

				line = in.readLine();
			}
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		} 		
	}

	@Override
	public ILista<VOPelicula> darListaPeliculas(String busqueda) {

		ILista<VOPelicula> resp = new ListaEncadenada<>(null);
		Iterator<VOPelicula> iter = misPeliculas.iterator();
		while(iter.hasNext()){
			VOPelicula actual = iter.next();
			if(actual.getTitulo().contains(busqueda.subSequence(0, busqueda.length()))){
				resp.agregarElementoFinal(actual);
			}
		}
		return resp;
	}

	@Override
	public ILista<VOPelicula> darPeliculasAgno(int agno) {

		Iterator<VOAgnoPelicula> iter = peliculasAgno.iterator();
		while(iter.hasNext()){
			VOAgnoPelicula actual = iter.next();
			if(actual.getAgno() == agno){
				return actual.getPeliculas();
			}
		}
		return null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoSiguiente() {
		if(peliculasAgno==null)
			return null;
		return(peliculasAgno.avanzarSiguientePosicion())? peliculasAgno.darElementoPosicionActual():null;
	}

	@Override
	public VOAgnoPelicula darPeliculasAgnoAnterior() {
		if(peliculasAgno==null)
			return null;
		return(peliculasAgno.retrocederPosicionAnterior())? peliculasAgno.darElementoPosicionActual():null;
	}

}
