package model.data_structures;

import java.util.Iterator;

public class ListaDobleEncadenada<T> implements ILista<T> {

	private int size;
	private NodoDoble<T> primero;
	private NodoDoble<T> ultimo;
	private NodoDoble<T> actual;

	public ListaDobleEncadenada(NodoDoble<T> pPrimero){
		size = 0;
		primero = pPrimero;
		actual = primero;

		ultimo = primero;
		if(ultimo!=null){
			size = 1;
			while(ultimo.darSiguiente()!=null){
				ultimo = ultimo.darSiguiente();
				size++;
			}
		}
	}

	protected class MiIterator implements Iterator<T>   {

		public MiIterator(){

		}
		public boolean hasNext(){
			actual = (actual!=null) ? actual.darSiguiente() : primero;
			return actual!=null;
		}
		public T next() {
			avanzarSiguientePosicion();
			return (actual == null)? null:actual.darElemento();
		}
		@Override
		public void remove() {/** NO APLICA**/}
	}

	@Override
	public Iterator<T> iterator() {
		Iterator<T> iter = new MiIterator();
		return iter;
	}

	@Override
	public void agregarElementoFinal(T elem) {
		NodoDoble<T> nuevo = new NodoDoble<>(elem);
		if(primero == null){
			primero  = nuevo;
		}
		else{
			ultimo.cambiarSiguiente(nuevo);
			nuevo.cambiarAnterior(ultimo);
		}
		ultimo = nuevo;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		int i =0;
		T resp = null;
		if (primero!= null){
			resp = primero.darElemento();
			actual = primero;
			while(i<pos){
				actual = actual.darSiguiente();
				i++;
			}
			resp = actual.darElemento();
		}
		return resp;
	}

	@Override
	public int darNumeroElementos() {
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		return (actual == null)? null:actual.darElemento();
	}

	@Override
	public boolean avanzarSiguientePosicion() {

		if(actual!=null && actual.darSiguiente()!=null){
			actual = actual.darSiguiente();
			return true;
		}else return false;
	}

	@Override
	public boolean retrocederPosicionAnterior() {

		if(actual!=null && actual.darAnterior()!=null){
			actual = actual.darAnterior();
			return true;
		}else return false;
	}
}
