package model.data_structures;

public class NodoSencillo<T> {

	private T elem;
	private NodoSencillo<T> siguiente;
	private int posicion;
	
	public NodoSencillo(T pElem, NodoSencillo<T> pSiguiente, int pPosicion)
	{
		elem = pElem;
		siguiente = pSiguiente;
		posicion = pPosicion;
	}
	
	public T darElemento()
	{
		return elem;
	}
	
	public NodoSencillo<T> darSiguiente()
	{
		return siguiente;
	}
	
	public int darPosicion()
	{
		return posicion;
	}
	
	public void cambiarSiguiente (NodoSencillo<T> nuevoSiguiente)
	{
		siguiente = nuevoSiguiente;
	}
	
}
