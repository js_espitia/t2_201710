package model.data_structures;

import java.util.Iterator;

public class ListaEncadenada<T> implements ILista<T> {

	private NodoSencillo<T> actual;
	private NodoSencillo<T> primero;
	private NodoSencillo<T> ultimo;
	private int size;


	public ListaEncadenada(NodoSencillo<T> pPrimero) {
		// TODO Auto-generated constructor stub
		actual = primero;
		primero = pPrimero;
		ultimo = primero;
		size = 0;
		if(ultimo!=null){
			size = 1;
			while(ultimo.darSiguiente()!=null)
			{
				size++;
				ultimo = ultimo.darSiguiente();
			}
		}
	}

	protected class miIterador implements Iterator<T>
	{
		protected int pos;
		public miIterador()
		{
			pos =0;
		}
		public boolean hasNext()
		{
			actual = (actual!=null) ? actual.darSiguiente() : primero;
			return actual!=null;
		}

		public T next()
		{
			avanzarSiguientePosicion();
			return (actual == null)? null:actual.darElemento();
		}
		
		@Override
		public void remove() {
			// TODO Auto-generated method stub
			//nope
		}
	}

	@Override
	public Iterator<T> iterator() {
		// TODO Auto-generated method stub
		Iterator<T> iter = new miIterador();
		return iter;
	}



	@Override
	public void agregarElementoFinal(T elem) {
		// TODO Auto-generated method stub
		NodoSencillo<T> n = new NodoSencillo<T>(elem, null, size);
		if(ultimo == null)
		{
			primero = n;
		}
		else
		{
			ultimo.cambiarSiguiente(n);
		}
		ultimo = n;
		size++;
	}

	@Override
	public T darElemento(int pos) {
		// TODO Auto-generated method stub
		if (actual == null)
			return null;
		else if(actual.darPosicion() == pos)
			return actual.darElemento();
		else
		{
			actual = primero;
			while(actual.darSiguiente()!=null)
			{
				if(actual.darPosicion()==pos)
				{
					return actual.darElemento();
				}
				actual = actual.darSiguiente();
			}
		}
		return null;
	}


	@Override
	public int darNumeroElementos() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public T darElementoPosicionActual() {
		// TODO Auto-generated method stub
		if(actual!=null)
			return actual.darElemento();
		else
			return null;
	}

	@Override
	public boolean avanzarSiguientePosicion() {
		// TODO Auto-generated method stub
		if (actual.darSiguiente()!=null)
		{
			actual = actual.darSiguiente();
			return true;
		}
		else
			return false;

	}

	@Override
	public boolean retrocederPosicionAnterior() {
		// TODO Auto-generated method stub
		if(actual == primero|| actual == null)
			return false;
		NodoSencillo<T> inicial = actual;
		actual = primero;
		while(actual.darSiguiente()!=null){
			if(actual.darSiguiente() == inicial){
				return true;
			}
		}
		return false;
	}
}
