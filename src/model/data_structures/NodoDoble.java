package model.data_structures;

public class NodoDoble <T> {
	private T elemento;
	private NodoDoble<T> anterior;
	private NodoDoble<T> siguiente;

	public NodoDoble ( T pElemento){
		elemento = pElemento;
		siguiente = null;
		anterior = null;
	}

	public T darElemento(){
		return elemento;
	}
	public NodoDoble<T> darAnterior(){
		return anterior;
	}
	public NodoDoble<T> darSiguiente(){
		return siguiente;
	}
	public void cambiarAnterior(NodoDoble<T> nuevoAnterior){
		anterior = nuevoAnterior;
	}
	public void cambiarSiguiente(NodoDoble<T> nuevoSiguiente){
		siguiente = nuevoSiguiente;
	}
}
