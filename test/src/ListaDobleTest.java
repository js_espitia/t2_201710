import model.data_structures.ListaDobleEncadenada;
import model.data_structures.NodoDoble;
import junit.framework.TestCase;


public class ListaDobleTest extends TestCase{
	private ListaDobleEncadenada<String> lista;

	private void setupEscenario1(){
		lista = new ListaDobleEncadenada<String>(new NodoDoble<String>( "Primero"));
	}
	private void setupEscenario2(){

		lista = new ListaDobleEncadenada<String>(new NodoDoble<String>("1"));
		lista.agregarElementoFinal("2");
		lista.agregarElementoFinal("3");

	}

	public void testDarElemPosicionActual(){
		setupEscenario1();
		assertEquals(lista.darElementoPosicionActual(), "Primero");
	}
	
	public void testAgregarFinal(){
		setupEscenario1();
		lista.agregarElementoFinal("Segundo");
		lista.agregarElementoFinal("Tercero");

		assertEquals(lista.darElemento(1), "Segundo");
		assertEquals(lista.darElemento(2), "Tercero");
		assertTrue(lista.darNumeroElementos()==3);
	}

	public void testAvanzar(){
		setupEscenario1();
		assertFalse(lista.avanzarSiguientePosicion());
		lista.agregarElementoFinal("Segundo");
		lista.darElemento(0);
		assertTrue(lista.avanzarSiguientePosicion());
	}

	public void testRetroceder(){
		setupEscenario1();
		assertFalse(lista.retrocederPosicionAnterior());

		lista = null;

		setupEscenario2();
		assertEquals(lista.darElementoPosicionActual(),"1");
		lista.darElemento(2);
		System.out.println(lista.darElementoPosicionActual());
		assertTrue(lista.retrocederPosicionAnterior());
		assertEquals(lista.darElementoPosicionActual(), "2");

	}


}
